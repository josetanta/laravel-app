@extends('layouts.container')

@section('title', 'Comentarios')

@section('container')
<h2 class="text-emerald-500 text-2xl font-bold my-5">Comentarios</h2>

<div class="container flex justify-between items-center gap-5">
	<form method="POST" action="" class="flex flex-col gap-5 shadow p-7">
		@csrf
		<input name="content" type="text" placeholder="Ingrese un comentario" required class="p-2 outline-none bg-gray-200 rounded">
		<button type="submit" class="text-center px-4 py-2 rounded shadow bg-amber-100 hover:bg-amber-200 transition-colors">Enviar</button>
	</form>

	<div class="container">
		<h4 class="text-xl font-semibold text-center text-amber-600">Comentarios</h4>
		<ul class="p-2 bg-gray-100/20 block list-decimal list-inside">
			@forelse($comments as $comment)
			<li class="p-2 bg-blue-100 my-2 rounded">{{ $comment->content }}</li>
			@empty
			<span class="text-rose-400 font-bold">NO hay comentarios</span>
			@endforelse
		</ul>
	</div>
</div>
