<head>
	<title>App - @yield('title', 'Home')</title>
	@vite('resources/css/app.css')
</head>

<header>

</header>

<main class="container">
	@yield('container')
</main>
